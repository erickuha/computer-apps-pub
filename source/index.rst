.. Computer Apps Primer documentation master file, created by
   sphinx-quickstart on Fri Oct 27 13:57:25 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Computer Apps Primer
====================

Introduction
------------

We live in an age of ubiquitous digital technology. Computers are in our cars, our homes, our schools. Many of us carry around a palm-sized computer that is orders of magnitude more powerful than the ones that took astronauts to the moon. And despite all of that, they can be a daunting challenge to understand and master. And despite that, a basic understanding of the conventions, methods, purposes, and operation of computers is more important than ever.

This living text will cover the basic use of computer software. Topics will include history and impact of computers, introduction to basic software on a computer, focusing on productivity software \(Microsoft Office, LibreOffice, and Google Cloud\). A major portion of the text covers the four main productivity paradigms: word processing, spreadsheets, presentations graphics, and databases. Also covered will be e-mail, basic internet use, search engines, cloud-based software, mobile applications, and digital ehtics.

A PDF version of this text can be found `here <http://itech.erickuha.com/ComputerAppsPrimer.pdf>`_.


Table of Contents
-----------------

.. toctree::
   :maxdepth: 1
   
   book/ch1-computer-basics/introduction
   book/ch2-word-processing/introduction
   book/ch3-presentation/introduction
   book/ch4-spreadsheets/introduction
   book/ch5-databases/introduction
   book/ch6-internet/introduction
   book/ch7-other/introduction
