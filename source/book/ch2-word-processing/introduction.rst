Word Processing
---------------

Word processing software's purpose is to allow the user to create professional-looking documents like letters, flyers, and brochures.

Typesetting as a practice is as old as the written word. But modern typesetting began with the printing press and many of the principles the developed in the early days of the printing press still apply to modern word processing. It wasn't until the 1960s that word processing by computers and software began to be explored. The idea was to duplicate what a typewriter does, only in software. They've come a long way since IBM's first word processor Selectric typewriter. Microsoft Word will be the primary focus of this chapter.

The computers on campus run Word 2013, however, many of the screenshots in this section will be of Word 2016. The differences are largely cosmetic, however, and where functional differences exist, they are usually behind the scenes.

Getting Started
~~~~~~~~~~~~~~~

.. toctree::
    :maxdepth: 1

    word_tour

Tutorials
~~~~~~~~~

.. toctree::
    :maxdepth: 1

    tutorial_creating_editing_saving
    tutorial_basic_text
    tutorial_formatting_images
    tutorial_styles_themes
    tutorial_mla

Projects
~~~~~~~~

.. toctree::
    :maxdepth: 1

    project_block_letter
    project_flyer
    project_resume

