Lesson 1: Testing
-----------------

Let's take off and land

#. Once you're up and ready to go. Look at the interface. On the left hand side are the blocks that we will use to program the drone.
#. There are five categroies that we will use:

   .. image:: images/categories.png
       :width: 50%

#. Starting with a blank program, add the *on start* block.

   .. image:: images/start.png
       :width: 50%

#. Add a *takeoff* block by dragging with your finger. Once you get it positioned, it will snap into place creating a short program.

   .. image:: images/takeoff.png
       :width: 50%

#. Add a *land* block to the end of the program.

   .. image:: images/takeoffland.png
       :width: 50%

#. Finally, start the program:

   .. image:: images/interface.png
       :width: 100%

Try this:

* Add a *stop* block to the end of the program. What does it do?
* Make the drone repeat the progam more than once.
* Experiment further with the software and add different combinations of take off, land, and stop.
