The File Manager
----------------

There's a lot to learn about using a computer and its applications. One of the most important applications on a computer is the file manager app. In Windows, this app is called the Windows Explorer or just Explorer.

Terms
~~~~~


Hard Drive
    A computer input/output device capable of storing data for long periods of time. It consists of platters covered with magnetic dust. The orientation of those magnets determines the content of that data. Some modern Hard Drives are called Solid-State Drives which are far faster (but more expensive) and do not use mechanical platters.
File
    A file is a named resource stored on a computer hard drive.
Folder
    A folder is a an organizational data structure that allows you to group files together into logical places and themes. For example, you might store all of your files for this class in a single folder so that you always know where they are.

What is a File System?
~~~~~~~~~~~~~~~~~~~~~~

The File System is part of an OS that manages long-term storage of data on a computer. This data is stored a device called a hard drive. Today's hard drives can store astonishing amounts of data. Briefly, consider that the most basic data storage unit on a computer is called a binary digit, or a bit. A bit is typically represented as either a 1 or 0. It's on or off. Clever mathematicians and computer scientists have developed ways of using long strings of bits to represent just about anything, from an email, to a video file to an entire operating system. At the very core, the only thing your computer ever does is manipulate staggeringly long strings of 1s and 0s.

Traditionally, it takes 8 bits to represent, say, a letter of the alphabet. Today, a modern hard drive in a new computer is typically one or two terabytes worth of storage. That's approximately 8,000,000,000,000 bits. 8 trillion bits. This is enough to store almost 4 million image files, around 300 hours of video, or a thousand copies of an old style encyclopedia. Ten terabytes can hold the entire printed collection of the Library of Congress. It is estimated that the combined storage for the biggest tech companies (Google, Amazon, Microsoft, Facebook) is around 1,200 **petabytes** or 1.2 *million* terabytes. For more information, `check here <http://www.whatsabyte.com/>`_.

There are many different ways that a computer can divide all those bits and bytes into space for individual files. And for our purposes, we don't really need to go into all of this. That said, if you would like to learn more about file systems, `Wikipedia <https://en.wikipedia.org/wiki/File_system>`_ is a good place to start.

Most modern computers use a `file and folder metaphor <https://en.wikipedia.org/wiki/Directory_(computing)>`_ to organize the data on a computer. While a computer directory system is quite a bit more complex than a filing cabinet, it is very useful to think about it as an infinitely deep file cabinet because it's easier for us to understand.

Let's look at how Windows does this.

TUTORIAL - The Windows Explorer App
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    **Note:** Make a note of the ways that your computer differs from what you see below.


1. Open your Windows Explorer app by clicking on the folder icon on the system task bar:

    .. figure:: images/task_bar_file_highlighted.png
       :width: 75%

       The File Manager Icon

    Alternately, you can click on the **Windows Start** Button and then on the **My Computer** button:

    .. figure:: images/task_bar_windows_button_my_computer.png
        :width: 75%

        The Start Menu

2. The application window will open and should look something like this:

    .. figure:: images/file_system_1.png
        :width: 75%

        The Windows Explorer App
        

4. Take a look around. There are three main areas of the window. The left sidebar is your **navigation pane** which allows you to quickly jump to common and important folders in your computer's file system. Get to know these links, as they can be very useful if you use them. Pay particular attention to the **Libraries** folders. Among your Libraries is the **Documents** folder which you should make a habit of using to store all of your working files so that they cannot get lost. In fact, on our campus, the Documents library is linked to your account and is accessible from any computer on campus. The main pane of the window is where you'll see all of the files and folders contained in your active directory. You might see something different depending on how your computer is configured. Above that, you will see the **address bar** which tells you what part of the computer's file system you are looking at. In the screenshot above, it is looking at the top, root level of the computer. Below the address bar, you will see a tool bar that will change depending on where you are in the computer. Pay attention to what tools are available as you navigate through the file manager. Next, at the bottom, you will see the **Details Pane** which shows details about the currently selected item. This might include meta data about a file such as when it was created, how large it is, and who the author is. It might also tell you how much free hard drive space you have. Last, in the top right, there is the **Search Box** which can be very useful to find a particular file in the the current folder.

5. Click on the **Documents** folder in the left navigation pane.

    .. figure:: images/documents_folder.png
        :width: 75%

        The Documents Folder

   This is my Documents folder at my workstation at work. Notice that there are many folders for all sorts of different aspects of my job (and hobbies).

6. **Right-click** in some empty space inside this folder. A context menu will appear (note, right-clicking on something in Windows will always bring up a context menu).
7. **Left-click** on **New** and from the new menu that pops up, click on **Folder**. A new folder will appear in your Documents library and the name "New Folder" will be highlighted.
8. Type :code:`ITECH 100` and press **Enter**. You have now created a folder to store all of your files for this class.
9. **Double-click** on the new folder. You are now inside of your new folder and the first thing you will notice, of course, is that it is empty. Let's remedy that.

10. **Right-click** somewhere in the folder. Click on **New** and then click on **Microsoft Word Document**

   .. figure:: images/new_file.png
       :width: 75%

       Creating a New Word Document

11. **Double-Click** on the new file. Microsoft Word should open and your new empty file should open automatically.

12. Type some text into the file. Anything, really. Whatever you like. Just get something on the page. We will go into great detail about Word's capabilities in the next chapter, however, for now, anything will do. 
13. To save the Word Document, left-click on the **File** tab at the top-left of the screen. In the backstage menu that appears, select **save**.

   .. figure:: images/word_doc.png
       :width: 75%

       The Microsoft Word Interface

14. Close the file and the app by clicking the **X** button in the top-right corner.

15. Close all other apps and windows. 
    
16. feel free to explore some of the other files. See if you can find anything interesting. If anything seems strange or arcane, see if you can figure out what it does or what it's for.


Closing Thoughts
~~~~~~~~~~~~~~~~

It is important for your own sanity that decide early on a system for organizing your files and then stick to it. There is nothing more frustrating that not knowing where you saved a file or worse, never being able to find it again. A simple file organization scheme for a student might be to create a folder for each class in your Documents library so that you know where they are all located quickly. So learn to use the Explorer app, organize your files, and if you have any questions *ask them*.

Up Next
~~~~~~~

In the next section, we will look at what productivity software is and what it's for and compare a few examples.
