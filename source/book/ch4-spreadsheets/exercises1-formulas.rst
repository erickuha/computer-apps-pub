PROJECT - Formulas Practice
---------------------------

This is a quick one. There are two scenarios presented in the workbook. On the first, you have a spreadsheet upon which your friend has kept track of all of thier bills for the last year. The only problem is, they are not entirely certain how to add it all up to see how much they paid over the course of the year. You will fill in the missing pieces using all that you have learned.

In the second scenario, your co-worker brings you a requisition form on a spreadsheet showing the meat quantities for a company barbecue. Only problem is, your co-worker never took this class and has no idea how to calculate the totals in the spreadsheet. That's where you come in.

#. Download the `start file <http://erickuha.com/primer/excel_resources/formulas_start.xlsx>`_
#. In the **Bills** sheet, fill in the totals column and the totals row using whatever formulas and functions you feel are appropriate.
#. In the **Requisition** sheet, fill in the blank spaces in the sheet as logically as you can. You will work quickly if you use basic **formulas**, the **fill handle**, and probably only one **function**.
#. Upload your resulting file to the class portal.
