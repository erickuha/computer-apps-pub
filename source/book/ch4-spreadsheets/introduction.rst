Spreadsheets
------------

The spreadsheet's purpose is to bring order to numerical data. With a spreadsheet app like Microsoft Excel, you can take raw data that is very hard for humans to parse and create readable, engaging, and useful charts, tables, and graphs. These graphs can be imported into any other document such as a PowerPoint presentation or a Word document.

The spreadsheet is the software that earned the computer its keep. The promise of being able to speed up the processing of tabular data is what convinced many corporations in the early days of computing to spend incredible sums of money on computers that, by today's standards, were very rudimentary. The modern spreadsheet application is kind of like a programming language in itself. Even with a basic understanding of tables, tabular data, and spreadsheet software you can make data entry and processing a far easier task and even impress potential employers.

**Note:** As always, these tutorials and projects will primarily use screenshots from Word 2016. If you are working with Word 2013, and something doesn't look the same or work the same way, please inform the instructor.

Getting Started
~~~~~~~~~~~~~~~

.. toctree::
    :maxdepth: 1

    excel_tour
    spreadsheet_theory

Tutorials
~~~~~~~~~

.. toctree::
    :maxdepth: 1

    tutorial1-1
    tutorial1-2
    tutorial2-formatting
    tutorial-references
    tutorial-functions-charts
    tutorial-advanced-functions
    tutorial_tables_pivot

Projects
~~~~~~~~

.. toctree::
    :maxdepth: 1

    exercises1-formulas
    project-pivot
    project-chart
    project_excel_review
    project_advanced_charting

Other
~~~~~

.. toctree::
    :maxdepth: 1

    review
