PROJECT - Advanced Weather Data
-------------------------------

In this project, you are largely on your own. You will be given a link to several interesting sets of data. It will be your job to look at what data there is, and see what was you can build interesting-looking charts from them. This is totally open-ended, but there are a few guidelines:

Directions
~~~~~~~~~~

`Data Files <http://erickuha.com/primer/excel_resources/weather/>`_

#. Each chart should be accompanied by a description of *why* this chart is interesting. However, brief it might be.
#. All of the files in the directory are :code:`.csv` files. This means that they must be converted to actual spreadsheet files *or* imported into Excel. Both of these ideas will be covered in class.
#. No low-effort submissions. You are going to build **five (5)** charts. That's not that many. Make sure each one has something interesting about it.
#. If you have an interesting idea for a chart, but are not sure how to implement that idea, *ask the instructor*. We'll figure out a way to get you where you want to go. Excel is powerful, but with all power comes complexity.
