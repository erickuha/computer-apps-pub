Databases
---------

A database is a collection of data. Good database software allows the user to add data, access it, and analyze that data and will have many built-in tools to make this quick and easy. They are perhaps the single most important piece of software that a computer possesses and most people are completely unaware of them.

For this course, we will focus on `Microsoft Access <https://products.office.com/en-us/access>`_, because it comes with a relatively easy-to-use built-in interface, but there are many, many other options out there of varying levels of complexity.

Theory
~~~~~~

.. toctree::
    :maxdepth: 1

    fundamentals
    elements

Tutorials
~~~~~~~~~

.. toctree::
    :maxdepth: 1

    tutorial-tables
    tutorial-data
    tutorial-forms
    tutorial-queries
    tutorial-reports
