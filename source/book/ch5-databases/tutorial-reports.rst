TUTORIAL 5 - Reports
--------------------

In this tutorial, we will build the final piece part of the database.

Report
    A report is a well-formatted document suitable for printing. It can draw data from any part of a database, such as a query or table.

Tutorial
~~~~~~~~

Beethoven Report
""""""""""""""""

Create a report based on the Beethoven Query.

Full Report
"""""""""""

Create a report that pulls all of the data from the entire database.

Create One More Report
""""""""""""""""""""""

Create one more report from any query(s) or table(s) of your choice.
