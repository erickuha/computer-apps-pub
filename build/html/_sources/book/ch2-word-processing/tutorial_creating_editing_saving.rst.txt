Tutorial 1 - Creating, Editing, and Saving a Word Document
----------------------------------------------------------
We are going to create, edit, and save a word document. This document will be submitted to the class portal for grading.

TUTORIAL
~~~~~~~~




#. Open Microsoft Word by clicking the **Start Menu** button and either navigating to Word or typing "word" into the search bar.

#. Word will open into the **backstage view**.

   .. figure:: images/tutorial1/new_document.png
      :alt: New Document

      Backstage View

#. Click on the **Blank Document** template.

#. In the document, type out a brief introduction for yourself answering these questions:

   * What is your name?
   * What is your program?
   * Do you own a general-purpose computer? What kind? (Smartphones count)
   * What do you use it for?
   * Do you use a computer at work?
   * What do you use it for?
   * On a scale of 1-10, how would you rate your experience with digital technology?
   * What do you hope to get out of this class?

   .. figure:: images/tutorial1/intro_example.png
       :alt: Intro Example

       This is kind of what we're looking for.

#. When you are finished typing, click the **File** tab to return to the **backstage view**.

#. Click **Save As**

   .. figure:: images/tutorial1/save_as.png
       :alt: save as

       Click "Save As"

#. Click **Browse** to open the file explorer.
#. Navigate to your itech 100 folder which you created in the file manager tutorial. Give this file a logical name. Ensure that it's being saved as a **Word Document (*.docx)**. Click **Save**.

   .. figure:: images/tutorial1/explorer.png

       Your "Save As" dialog should look kind of like this.

#. You can exit Word now. Finally, you will upload this file to class portal. Wait for instructions from the instructor for how to do this.


