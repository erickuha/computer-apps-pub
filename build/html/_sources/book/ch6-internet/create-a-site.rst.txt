Create a Web Site
-----------------

Building a website in the modern digital age is potentially quite easy. However, there are some pitfalls and challenges that must be overcome in order to make a website that is attractive to look at and worthwhile to visit.

In this ongoing project, you will be building and maintaining a personal web site from scratch using a powerful, long-established web software suite called `Wordpress <https://wordpress.com/>`_. This website will be part blog and part research journal for the material we cover in this course. Some of the things you write will be completely open-ended, and others will be prompted. None of them will take more than 10 or 15 minutes to complete (in theory).

Also, throughout the term, we will learn about some of WordPress's more advanced features, but for this first assignment, you should just use the basic functionality that you can see.

Your First Task
~~~~~~~~~~~~~~~

1. Create a website using WordPress.
2. Create your first "**post**".
3. In this post, introduce yourself, and answer a few questions

   * What's your program
   * What do you hope to get out of this course
   * Is there any particular app or piece of software that you would like to look at in this course?

4. Include an image/meme/other multimedia if you like.

Submit the URL to your site to course portal on Canvas.

